package ru.t1.ktubaltseva.tm.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@ComponentScan("ru.t1.ktubaltseva.tm")
public class ApplicationConfiguration {
}
